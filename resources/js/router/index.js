import Vue from 'vue'
import VueRouter from 'vue-router'
import LandingPage from '../views/LandingPage.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'LandingPage',
    component: LandingPage
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue'),
    children: [
        {
            path: 'konfigurasi',
            component: () => import('../components/main/konfigurasi')
        },
        {
            path: 'dashboard',
            component: () => import('../components/main/dashboard')
        },
        {
            path: 'setting',
            component: () => import('../components/main/setting')
        },
        {
            path: 'admisi',
            component: () => import('../components/main/admisi'),
            children: [
                {
                    path: '/',
                    component: () => import('../components/main/admisi/Overview.vue'),
                }
            ]
        },
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router