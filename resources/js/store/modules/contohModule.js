import axios from 'axios'

//tempat utk inisialisasi variable
const state = {
    todos: []
};

//self explanatory
const getters = {
    allTodos: (state) => state.todos
};

//tempat bwt panggil API, CRUD, dll
const actions = {
    async fetchTodos({ commit }) {
        const response = await axios.get('https://jsonplaceholder.typicode.com/todos');
        //manggil function di mutations
        commit('setTodos',response.data);
    },
    async addTodo({ commit }, title) {
        const response = await axios.post('https://jsonplaceholder.typicode.com/todos',{
            title, completed: false
        });
        commit('newTodo',response.data);
    },
    async deleteTodo({ commit }, id) {
        await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);
        commit('removeTodo', id);
    },
    async filterTodos({ commit }, e) {
        //get sleected idx
        const limit = parseInt(e.target.options[e.target.options.selectedIndex].innerText);
        const response = await axios.get(`https://jsonplaceholder.typicode.com/todos?_limit=${limit}`);
        //manggil function di mutations
        commit('setTodos',response.data);
    },
    async updateTodo({ commit }, upd) {
        const response = await axios.put(`https://jsonplaceholder.typicode.com/todos/${upd.id}`, upd);
        //manggil function di mutations
        commit('updTodo', response.data);
    }
};

//yang merubah state ( dipanggil di actions melalui method commit() )
const mutations = {
    setTodos: (state, todos) => (state.todos = todos),
    newTodo: (state, todo) => state.todos.unshift(todo),
    removeTodo: (state, id) => (state.todos.filter(t => t.id !== id)),
    updTodo: (state, upd) => {
        let idx = state.todos.findIndex(t => t.id === upd.id);
        if(idx !== -1) {
            state.todos.splice(idx, upd);
        }
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}

// Vuex boilerplate

// const state = {};
// const getters = {};
// const actions = {};
// const mutations = {};

// export default {
//     state,
//     getters,
//     actions,
//     mutations
// }