import Vue from 'vue'
import Vuex from 'vuex'
import contohModule from './modules/contohModule'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        contohModule
    }
})