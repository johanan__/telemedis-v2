import Vue from 'vue'
import App from './app.vue'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap'

new Vue({
    router,
    render: h => h(App)
  }).$mount('#app')